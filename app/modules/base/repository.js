class Repository {

    constructor(model) {
        this.model = model;
    }

    get(criteria) {
        return this.model.find(criteria);
    }
    
    findOne(criteria) {
        return this.model.findOne(criteria);
    }

    findById(id) {
        return this.model.findById(id);
    }

    deleteById(id) {
        return this.model.findOneAndDelete(id);
    }

    update(criteria, data) {
        return this.model.update(criteria, data);
    }

    updateOne(criteria, data) {
        return this.model.updateOne(criteria, data);
    }

    updateById(id, data) {
        return this.updateOne({_id: id}, data);
    }

    async saveOrFind(data) {
        const isExists = await this.model.findOne(data)

        if (isExists) {
            return isExists;
        }

        return this.save(data);
    }

    save(data) {
        return this.model.create(data);
    }

}

module.exports = Repository;
