class CrudController  {
    constructor(repository, folder) {
        this.repository = repository;
        this.folder = folder;
    }

    async index(req, res, next) {
        const criteria = req.body;

        const results = await this.repository.get(criteria);

        res.render(`${this.folder}/index`, results);
        next();
    }

    async store(req, res, next) {
        const data = req.body;

        const result = await this.repository.save(data);

        res.redirect(req.originalUrl);
        next();
    }

    async show(req, res, next) {
        const id = req.params.id;

        const result = await this.repository.findById(id);

        res.render(`${this.folder}/show`, result);
        next();
    }

    async edit(req, res, next) {
        const id = req.params.id;

        const result = await this.repository.findById(id);

        res.render(`${this.folder}/edit`, result);
        next();
    }

    async update(req, res, next) {
        const id = req.params.id;
        const data = req.body;

        const result = await this.repository.updateById(id, data);

        res.redirect(req.originalUrl);
        next();
    }

    async delete(req, res, next) {
        const id = req.params.id;

        const result = await this.repository.deletById(id);

        res.redirect(req.originalUrl);
        next();
    }
}

module.exports = CrudController;
