const USER_DATE_SCHEMA = {
    user: {
        type: String,
        required: true,
    },
    when: {
        type: Date,
        required: true,
    }
};


module.exports = {
    USER_DATE_SCHEMA,
};
