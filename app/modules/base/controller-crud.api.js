let repository;

class CrudController  {
    repository = null;

    constructor(_repository) {
        repository = _repository;
    }
    
    async index(req, res, next) {
        const criteria = req.body;

        const results = await repository.get(criteria);

        res.status(200).send(results);
        next();
    }

    async store(req, res, next) {
        const data = req.body;

        const result = await repository.save(data);
        
        res.status(201).send(result);
        next();
    }

    async show(req, res, next) {
        const id = req.params.id;

        const result = await repository.findById(id);
        
        res.status(200).send(result);
        next();
    }

    async update(req, res, next) {
        const id = req.params.id;
        const data = req.body;

        const result = await repository.updateById(id, data);
        
        res.status(201).send(result);
        next();
    }

    async delete(req, res, next) {
        const id = req.params.id;

        const result = await repository.deletById(id);
        
        res.status(201).send(result);
        next();
    }
}

module.exports = CrudController;