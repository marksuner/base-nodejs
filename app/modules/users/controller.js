const CrudController = require('../base/controller-crud.api');
const UserRepository = require('./repository');

class UsersController extends CrudController {
    constructor() {
        super(UserRepository);
    }
}

module.exports = new UsersController();
