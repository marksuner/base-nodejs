const Repository = require('../base/repository');

const User = require('./model');
const bcrypt = require('bcrypt');
const SALT_ROUNDS = 1;

class UserRepository extends Repository {
    constructor() {
        super(User);
    }

    async save(data = {}) {
        const hashed = await bcrypt.hash(data.password, SALT_ROUNDS);

        data.password = hashed;

        return this.model.create(data);
    }

    async update(criteria, data) {
        
        if (data.password) {
            data.password = await bcrypt.hash(data.password, SALT_ROUNDS);
        } else {
            delete data.password;
        }
        
        return this.model.update(criteria, data);
    }

    async findByUsernameAndPassword({username, password}) {
        const user = await this.model.findOne({
            username
        });

        if (!user) {
            return null;
        }

        const isValid = await bcrypt.compare(password, user.password);

        return !isValid ? null : user;
    }
}

module.exports = new UserRepository();
