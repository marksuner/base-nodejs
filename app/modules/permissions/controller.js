const CrudController = require('../base/controller-crud.api');
const PermissionRepository = require('./repository');

class PermissionController extends CrudController {
    constructor() {
        super(PermissionRepository);
    }
}

module.exports = new PermissionController();
