const express = require('express');
const router = express.Router();
const PermissionController = require('./controller');

router.get('/', PermissionController.index);
router.post('/', PermissionController.store);
router.get('/:id', PermissionController.show);
router.put('/:id', PermissionController.update);
router.delete('/:id', PermissionController.delete);

module.exports = router;
