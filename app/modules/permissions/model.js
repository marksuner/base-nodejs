const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { USER_DATE_SCHEMA } = require('../base/common.schema');

const permissionSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: false,
  },
  createdAt: {
    type: USER_DATE_SCHEMA,
    required: false,
  },
  updatedAt: {
    type: USER_DATE_SCHEMA,
    required: false,
  },
});

const Permission = mongoose.model('Permission', permissionSchema);

module.exports = Permission;
