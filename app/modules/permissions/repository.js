const Repository = require('../base/repository');

const Permission = require('./model');

class PermissionRepository extends Repository {
    constructor() {
        super(Permission);
    }
}

module.exports = new PermissionRepository();
