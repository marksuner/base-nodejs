const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { USER_DATE_SCHEMA } = require('../base/common.schema');

const roleSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: false,
  },
  permissions: [
      {
          type: Schema.Types.ObjectId,
          required: false,
      }
  ],
  createdAt: {
    type: USER_DATE_SCHEMA,
    required: false,
  },
  updatedAt: {
    type: USER_DATE_SCHEMA,
    required: false,
  },
});

const Role = mongoose.model('Role', roleSchema);

module.exports = Role;
