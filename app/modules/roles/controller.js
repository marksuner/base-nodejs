const CrudController = require('../base/controller-crud.api');
const RoleRepository = require('./repository');

class RoleController extends CrudController {
    constructor() {
        super(RoleRepository);
    }
}

module.exports = new RoleController();
