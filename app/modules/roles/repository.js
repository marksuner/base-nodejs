const Repository = require('../base/repository');

const Role = require('./model');

class RoleRepository extends Repository {
    constructor() {
        super(Role);
    }
}

module.exports = new RoleRepository();
