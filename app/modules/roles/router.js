const express = require('express');
const router = express.Router();
const RoleController = require('./controller');

router.get('/', RoleController.index);
router.post('/', RoleController.store);
router.get('/:id', RoleController.show);
router.put('/:id', RoleController.update);
router.delete('/:id', RoleController.delete);

module.exports = router;
