const oauth = require('./server');

const authenticate = async (req, res, next) => {
    try {
        const token = await oauth.authenticate(req, res);

        req.local.token = token;
        
        next();

    } catch(e) {
        throw new Error('Something went wrong', e);
    }
};

const authorize = async (req, res, next) => {
    try {
        const code = await oauth.authorize(req, res);

        res.local.oauth = {code};

        next();

    } catch(e) {
        throw new Error('Something went wrong', e);
    }
};

const token = async (req, res, next) => {
    try {
        const token = await oauth.token(req, res);

        req.local.oauth = {token};

        next();
    } catch(e) {
        throw new Error('Something went wrong', e);
    }
}

module.exports = {
    authenticate,
    authorize,
    token,
};
