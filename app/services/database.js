const config = require('config');
const mongoose = require('mongoose');

const Database = {
    connect() {
        return new Promise((resolve, reject) => {
    
            mongoose.connect(config.get('db.connection'), {useNewUrlParser: true});
    
            const connection = mongoose.connection;
        
            connection.once('open', () => {
                console.log('mongo connected')
                resolve(true);
            });
    
            connection.on('error', (e) => {
                reject(e);
            });
        });
    }
};

module.exports = Database;