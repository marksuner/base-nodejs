const UserRouter = require('./modules/users/router');
const RolesRouter = require('./modules/roles/router');
const PermissionRouter = require('./modules/permissions/router');

const express = require('express');
const router = express.Router();
 
// users router
router.use('/users', UserRouter);
router.use('/roles', RolesRouter);
router.use('/permissions', PermissionRouter);

module.exports = router;