describe('Permission Module', () => {
    require('./controller');
    require('./model');
    require('./repository');
});