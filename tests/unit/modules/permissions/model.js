const expect = require('chai').expect;

const Permission = require('../../../../app/modules/permissions/model');

describe('Permission model', () => {
    it('should be invalid if name is empty', (done) => {
        const model = new Permission();

        model.validate((err) => {
            expect(err.errors.name).to.exist;
            done();
        });
    });

});