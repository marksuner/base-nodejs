const expect = require('chai').expect;

const PermissionController = require('../../../../app/modules/permissions/controller');

describe('Permission controller', () => {

    it('should have index function', () => {
        expect(PermissionController.index).to.a('function');
    });
    
    it('should have store function', () => {
        expect(PermissionController.store).to.a('function');
    });

    it('should have show function', () => {
        expect(PermissionController.show).to.a('function');
    });

    it('should have update function', () => {
        expect(PermissionController.update).to.a('function');
    });
    
    it('should have delete function', () => {
        expect(PermissionController.delete).to.a('function');
    });
    
});