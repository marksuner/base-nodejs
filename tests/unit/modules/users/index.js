describe('User Module', () => {
    require('./controller');
    require('./model');
    require('./repository');
});