const expect = require('chai').expect;

const UserController = require('../../../../app/modules/users/controller');

describe('User controller', () => {

    it('should have index function', () => {
        expect(UserController.index).to.a('function');
    });
    
    it('should have store function', () => {
        expect(UserController.store).to.a('function');
    });

    it('should have show function', () => {
        expect(UserController.show).to.a('function');
    });

    it('should have update function', () => {
        expect(UserController.update).to.a('function');
    });
    
    it('should have delete function', () => {
        expect(UserController.delete).to.a('function');
    });
    
});