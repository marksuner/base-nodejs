const expect = require('chai').expect;

const User = require('../../../../app/modules/users/model');

describe('User model', () => {
    it('should be invalid if name is empty', (done) => {
        const model = new User();

        model.validate((err) => {
            expect(err.errors.name).to.exist;
            done();
        });
    });

    it('should be invalid if username is empty', (done) => {
        const model = new User();

        model.validate((err) => {
            expect(err.errors.username).to.exist;
            done();
        });
    });

    it('should be invalid if password is empty', (done) => {
        const model = new User();

        model.validate((err) => {
            expect(err.errors.password).to.exist;
            done();
        });
    });

});