const expect = require('chai').expect;

const Role = require('../../../../app/modules/roles/model');

describe('Role model', () => {
    it('should be invalid if name is empty', (done) => {
        const model = new Role();

        model.validate((err) => {
            expect(err.errors.name).to.exist;
            done();
        });
    });

});