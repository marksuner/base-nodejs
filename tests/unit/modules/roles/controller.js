const expect = require('chai').expect;

const RoleController = require('../../../../app/modules/roles/controller');

describe('Role controller', () => {

    it('should have index function', () => {
        expect(RoleController.index).to.a('function');
    });
    
    it('should have store function', () => {
        expect(RoleController.store).to.a('function');
    });

    it('should have show function', () => {
        expect(RoleController.show).to.a('function');
    });

    it('should have update function', () => {
        expect(RoleController.update).to.a('function');
    });
    
    it('should have delete function', () => {
        expect(RoleController.delete).to.a('function');
    });
    
});