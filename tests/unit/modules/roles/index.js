describe('Role Module', () => {
    require('./controller');
    require('./model');
    require('./repository');
});