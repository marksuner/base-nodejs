const expect = require('chai').expect

const Server = require('../../server');

describe("Server", () => {

  it('should expect to have a config path', () => {
    expect(Server.path).to.be.a('string');
  })

  it('should expect to have a config port', () => {
    expect(Server.port).to.be.a('number');
  })
  
  it('should expect to have a set template engine', () => {
    expect(Server.setTemplateEngine).to.be.a('function');
  })
  
  it('should expect to have an init function', () => {
    expect(Server.init).to.be.a('function');
  })
  
});