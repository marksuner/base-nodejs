const config = require('config');
const express = require('express');
const app = express();
const router = require('./router');
const Database = require('./app/services/database');
const hbs = require('express-handlebars');

const Server = {
    port: config.get('server.port'),
    path: config.get('server.path'),

    setTemplateEngine() {
        app.set('view engine', 'hbs');

        app.engine( 'hbs', hbs( {
          extname: 'hbs',
          defaultView: 'default',
          layoutsDir: __dirname + '/views/pages/',
          partialsDir: __dirname + '/views/partials/'
        }));
    },

    listen() {
        return new Promise((resolve, reject) => {
            Server.setTemplateEngine();

            router(app);

            app.listen(Server.port, () => {
                resolve(true);
            });
        });
    },

    init() {
        return Database.connect()
                .then(this.listen)
                .then(() => {
                    console.log(`${config.get('app.name')} app listening on port ${Server.port}!`)
                })
                .catch((e) => console.error(e));
    },
}

module.exports = Server;