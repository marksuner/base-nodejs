const Database = require('../app/services/database');

const seeders = [
  require('./UserCollectionSeeder'),
];

Database.connect()
    .then(() => {
        return Promise.all(seeders.map(seeder => seeder()));
    })
    .then((results) => {
        console.log("done seeding", results);
        process.exit();
    }).catch((e) => console.error('Something went wrong', e));