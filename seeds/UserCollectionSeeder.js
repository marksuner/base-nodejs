const mongoose = require('mongoose');

const ObjectId = mongoose.Types.ObjectId;

const UserRepository = require('../app/modules/users/repository');

const rolePermissionSeeder = require('./RolePermissionCollectionSeeder');

const UserCollectionSeeder = async () => {
    // wait for rolepermission seeder to finish
    const rolePermissionResults = await rolePermissionSeeder();

    const onGrabAdminId = () => {
        const role = rolePermissionResults.find((role) => role.name === 'Admin');

        return new ObjectId(role._id);
    }
    
    const users = [
        {
            username: 'admin',
            password: 'admin',
            name: 'Admin',
            role: onGrabAdminId(),
        }
    ];

    const saved = users.map((user) => UserRepository.saveOrFind(user));

    return Promise.all(saved);
}

module.exports = UserCollectionSeeder;