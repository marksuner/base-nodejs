const mongoose = require('mongoose');

const ObjectId = mongoose.Types.ObjectId;

const RoleRepository = require('../app/modules/roles/repository');
const PermissionRepository = require('../app/modules/permissions/repository');

const USER_PERM = {
    create: 'User create',
    read: 'User read',
    update: 'User update',
    delete: 'User delete',
}

const RolePermissionCollectionSeeder = async () => {
    const permissions = [
        {
            name: USER_PERM.create,
        },
        {
            name: USER_PERM.read,
        },
        {
            name: USER_PERM.update,
        },
        {
            name: USER_PERM.delete,
        },
    ];

    const savedPermissions = permissions.map((permission) => PermissionRepository.saveOrFind(permission));

    const resultPermissions = await Promise.all(savedPermissions)

    const onGrabPermissionId = (name) => {
        const _id = resultPermissions.find((resultPermission) => resultPermission.name === name)._id;

        return new ObjectId(_id);
    }

    const roles = [
        {
            name: 'Admin',
            description: 'Administrator',
            permissions: [
                onGrabPermissionId(USER_PERM.create),
                onGrabPermissionId(USER_PERM.read),
                onGrabPermissionId(USER_PERM.update),
                onGrabPermissionId(USER_PERM.delete),
            ],
        }
    ];

    const savedRoles = roles.map((role) => RoleRepository.saveOrFind(role));

    return Promise.all(savedRoles);
}

module.exports = RolePermissionCollectionSeeder;