const V1Routes = require('./app/route.v1');

const globalMiddlewares = [
    // 
];

module.exports = (app) => {

    globalMiddlewares.forEach((middleware) => {
        app.use('*', middleware);
    });

    // route version 1.0.0
    app.use('/v1', V1Routes);

    // if all routes fail, send status 404
    app.use('*', (req, res) => {
        res.status(404).send('Route not found');
    });
};
